<?php
/**
 * Created by IntelliJ IDEA.
 * User: gayan
 * Date: 5/25/18
 * Time: 4:32 PM
 */
?>
@extends('admin_template')

@section('content')
    <div class="container-fluid">
        <div class="card small">
            <div class="card-header">
                <div class="card-title text-info text-uppercase">
                    LOCATION LIST
                    <a href="/location/create" target="_self">
                        <button type="button" class="btn btn-success btn-sm float-right "><i class="fa fa-plus"> New
                                Location</i>
                        </button>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <form action="/location/search" method="post">
                    <div class="form-group row">
                        {{csrf_field()}}
                        <div class="col-lg-6">
                            <input type="text" name="search" class="form-control" placeholder="SEARCH">
                        </div>
                        <div class="col-lg-3">
                            <button type="submit" class="btn btn-primary btn-sm float-left"><i
                                        class="fa fa-search">Search</i></button>
                        </div>
                    </div>
                </form>
                @if (session('alert'))
                    <div class="alert alert-success">
                        <button type="button"
                                class="close"
                                data-dismiss="alert"
                                aria-hidden="true">&times;
                        </button>
                        {{ session('alert') }}
                    </div>
                @endif
                <div class="form-group row">
                    @if(isset($locations))
                        <table class="table table-responsive-lg">
                            <thead class="bg-navy">
                            <tr>
                                <th>#</th>
                                <th>NAME</th>
                                <th>CONTACT PERSON</th>
                                <th>TELEPHONE</th>
                                <th>EMAIL</th>
                                <th>ACTION</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($locations as $key=>  $location)
                                <tr>
                                    <td>{{$location->id}}</td>
                                    <td>{{$location->name}}</td>
                                    <td>{{$location->contact_person}}</td>
                                    <td>{{$location->telephone}}</td>
                                    <td>{{$location->email}}</td>
                                    <td>
                                        <button class="btn btn-default btn-sm" data-toggle="modal"
                                                data-target="#exampleModal_{{$key}}"
                                                data-whatever="@mdo" id="view_{{$key}}">view</button>
                                    </td>
                                </tr>
                                <div class="modal fade" id="exampleModal_{{$key}}" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">

                                        <div class="modal-content">
                                            <form action="/location/update" method="post">
                                                @csrf
                                                <div class="modal-header bg-info">
                                                    <h5 class="modal-title" id="exampleModalLabel">Update Location</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body small">
                                                    <div class="form-group">
                                                        <label for="recipient-name"
                                                               class="col-form-label">NAME:</label>
                                                        <input type="text" class="form-control"
                                                               name="name"
                                                               id="name_{{$key}}" value="{{$location->name}}"
                                                               required>
                                                        <input type="hidden" value="{{$location->id}}" name="id">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="col-form-label">CONTACT
                                                            PERSON:</label>
                                                        <input type="text" class="form-control" id="contact_person"
                                                               value="{{$location->contact_person}}" required
                                                               name="contact_person">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="telephone"
                                                               class="col-form-label">TELEPHONE:</label>
                                                        <input type="number" class="form-control" id="telephone"
                                                               value="{{$location->telephone}}" required
                                                               name="telephone">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="address"
                                                               class="col-form-label">ADDRESS:</label>
                                                        <input type="text" class="form-control" id="address"
                                                               value="{{$location->address}}" required name="address">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="recipient-name"
                                                               class="col-form-label">EMAIL:</label>
                                                        <input type="text" class="form-control" id="email"
                                                               value="{{$location->email}}" name="email">
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                                                                class="fa fa-times"> Close</i></button>
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save">
                                                            Update</i></button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                        {!! $locations->render() !!}
                    @else
                        <div class="alert alert-warning col-lg-12 text-center" role="alert">
                            <span>{{ $message }}</span>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <script>
        $('div.alert').delay(2000).slideUp(300);
    </script>
@endsection
