<?php
/**
 * Created by IntelliJ IDEA.
 * User: gayan
 * Date: 7/5/18
 * Time: 10:40 AM
 */ ?>
@extends('admin_template')
@section('content')
    <div class="container-fluid" ng-controller="grnCtrl" ng-app="grnApp">
        <div class="card small">
            <div class="card-header">
                <div class="card-title text-info text-uppercase">Good Receive Note List
                    <a href="/grn/index" target="_self">
                        <button type="button" class="btn btn-success btn-sm float-right "><i class="fa fa-plus"> New
                                Grn</i>
                        </button>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <form action="/grn/search" method="get">
                    <div class="form-group row">
                        {{csrf_field()}}
                        <div class="col-lg-6">
                            <input type="text" name="search" class="form-control" placeholder="SEARCH">
                        </div>
                        <div class="col-lg-3">
                            <button type="submit" class="btn btn-primary btn-sm float-left"><i
                                        class="fa fa-search">Search</i></button>
                        </div>

                    </div>
                </form>
                <div class="form-group row">
                    @if(isset($grns))
                        <table class="table table-bordered table-responsive-lg">
                            <thead class="bg-navy">
                            <tr class="text-uppercase">
                                <th>Grn No</th>
                                <th class="text-center">Location</th>
                                <th class="text-center">Type</th>
                                <th class="text-center">date</th>
                                <th class="text-center">status</th>
                                <th>action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($grns as $key =>$grn)
                                <tr>
                                    <td>{{$grn->id}}</td>
                                    <td class="text-center">{{$grn->name}}</td>
                                    <td class="text-center">
                                        @if($grn->grn_type ==GRN_TYPE_DIRECT) <span
                                                class="badge badge-pill badge-warning font-weight-bold">DIRECT</span>
                                        @elseif($grn->grn_type ==GRN_TYPE_GTN) <span
                                                class="badge badge-pill badge-info font-weight-bold">BY GTN</span>
                                        @endif
                                    </td>
                                    <td class="text-center">{{$grn->date}}</td>
                                    <td class="text-center">
                                        @if($grn->status ==PENDING_GRN) <span
                                                class="badge badge-pill badge-primary font-weight-bold">PENDING</span>
                                        @elseif($grn->status ==APPROVED_GRN) <span
                                                class="badge badge-pill badge-success font-weight-bold">APPROVED</span>
                                        @elseif($grn->status ==REJECTED_GRN) <span
                                                class="badge badge-pill badge-danger font-weight-bold">REJECTED</span>
                                        @endif
                                    </td>
                                    <td>
                                        <button class="btn btn-default btn-sm" ng-click="viewGrn({{$grn->id}})">view
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {!! $grns->render() !!}
                    @else
                        <div class="alert alert-warning col-lg-12 text-center" role="alert">
                            <span>{{ $message }}</span>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <script>

        var app = angular.module('grnApp', []);
        app.controller('grnCtrl', function ($scope, $http) {

            $scope.viewGrn = function (id) {
                window.open('/grn/view/' + id, '_blank');
            }
        })
    </script>
@endsection

